# Covid table

We're modeling a COVID infection in a  university's community using the [MESA framework](https://github.com/projectmesa/mesa). 

Heavily inspired in [Virus on network](https://github.com/projectmesa/mesa/tree/master/examples/virus_on_network)


### Model description
* External infections can appear when doing extracurricular activities.
* Each individual is added to a certain class at the time of its creation.
* Each individual can either wear or not a mask.

### Agent cycle
The model has the following 24h cycle (1 hour = 1 step)

0-7 AT_HOME (The individual is sleeping)

7-9 chance of AT_ACTIVITY else AT_HOME

9-14 AT_CLASS (Each individual has contact only with its class)

14-15 AT_CAFETERIA (All individuals can have contact with any other)

15-20 chance AT_ACTIVITY chance AT_CLASS else AT_HOME

20-23 chance AT_ACTIVITY else AT_HOME