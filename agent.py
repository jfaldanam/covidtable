import random
from math import sqrt

from mesa import Agent, Model

from state import HealthState, ActivityState

class Student(Agent):

    def __init__(
        self,
        unique_id: int,
        model: Model,
        x: int,
        y: int,
        initial_state: HealthState,
        initial_activity_state: ActivityState,
        class_number: int,
        use_mask: bool,
    ):
        super().__init__(unique_id, model)
        self.x = x
        self.y = y
        self.state = initial_state
        self.activity_state = initial_activity_state
        self.class_number = class_number
        self.use_mask = use_mask
        
        self.contacts = 0
        self.exposed_timer = 0

    def step(self):
        '''
        Planificate the action for this step.

        Update the location of the agent.
        '''
        # Reset number of contacts for the step
        self.contacts = 0

        hour = self.model.current_cycle_step % 24
        # Choose what to do on the next step
        if hour == 8:
            if self.random.random() < 0.1:
                self.activity_state = ActivityState.AT_ACTIVITY
            else:
                self.activity_state = ActivityState.AT_HOME
        elif hour == 10:
            self.activity_state = ActivityState.AT_CLASS
        elif hour == 14:
            self.activity_state = ActivityState.AT_CAFETERIA
        elif hour == 16 or hour == 18:
            if self.random.random() < 0.5:
                self.activity_state = ActivityState.AT_ACTIVITY
            elif self.random.random() < 0.5:
                self.activity_state = ActivityState.AT_CLASS
            else:
                self.activity_state = ActivityState.AT_HOME
        elif hour == 21:
            if self.random.random() < 0.5:
                self.activity_state = ActivityState.AT_ACTIVITY
            else:
                self.activity_state = ActivityState.AT_HOME
        elif hour == 23:
            self.activity_state = ActivityState.AT_HOME
        

    def advance(self):
        '''
        Execute the action where the agent is located in this step
        '''
        if self.activity_state is ActivityState.AT_HOME:
            self.at_home()
        elif self.activity_state is ActivityState.AT_CLASS:
            self.at_class()
        elif self.activity_state is ActivityState.AT_CAFETERIA:
            self.at_cafeteria()
        elif self.activity_state is ActivityState.AT_ACTIVITY:
            self.do_activity()

        # Check if a Exposed individual incubates the virus
        self.incubate_virus()
        # Check if a Exposed individual incubation period has passed
        self.pass_incubation_period()
        # Check if a Infected individual cures itself of the virus
        self.cure_virus()
        # Check if a Infected individual dies
        self.am_i_dead()

    def at_home(self):
        '''
        A safe place when no infection can touch you
        '''
        pass

    def do_activity(self):
        '''
        A activity outside the scope of our model where a individual may get infected
        '''
        if self.state is HealthState.SUSCEPTIBLE:
            if self.random.random() < self.model.activity_infection_chance:
                self.state = HealthState.EXPOSED

    def at_class(self):
        '''
        Every individual is in contact with all individuals of its class
        '''
        neighbors = self.model.grid.get_neighbors((self.x, self.y), True)
        neighbors = [
            neighbor for neighbor in neighbors
            if neighbor.class_number == self.class_number
        ]
        self.get_exposed_to_virus(neighbors)

    def at_cafeteria(self):
        '''
        Every individual is in contact with all individuals
        '''
        neighbors = random.sample(self.model.get_agents(), self.model.cafeteria_table_size)
        neighbors = [
            neighbor for neighbor in neighbors
            if neighbor.unique_id is not self.unique_id
        ]
        self.get_exposed_to_virus(neighbors)

    def get_exposed_to_virus(self, neighbors):
        if self.state is HealthState.SUSCEPTIBLE:
            infected_neighbors = [
                neighbor for neighbor in neighbors
                if neighbor.state is HealthState.INFECTED
            ]

            for neighbor in infected_neighbors:
                self.contacts+=1
                dist = sqrt( (self.x - neighbor.x)**2 + (self.y - neighbor.y)**2 )
                mask_effect = 0
                if self.use_mask:
                    mask_effect += 0.3
                if neighbor.use_mask:
                    mask_effect += 0.4
                if self.random.random() < (self.model.virus_spread_chance*(1-mask_effect))/(dist**2):
                    self.state = HealthState.EXPOSED
                    break

    def incubate_virus(self):
        if self.state is HealthState.EXPOSED:
            if self.random.random() < (1/self.model.mean_incubation_time):
                self.state = HealthState.INFECTED
    
    def pass_incubation_period(self):
        if self.state is HealthState.EXPOSED:
            if self.exposed_timer >= self.model.mean_incubation_time:
                self.state = HealthState.SUSCEPTIBLE
            self.exposed_timer += 1

    def cure_virus(self):
        if self.state is HealthState.INFECTED:
            if self.random.random() < self.model.infection_recovery_chance:
                self.state = HealthState.SUSCEPTIBLE
                self.gain_virus_immnunity()

    def am_i_dead(self):
        if self.state is HealthState.INFECTED:
            if self.random.random() < self.model.lethal_infection_chance:
                self.state = HealthState.RESISTANT
                self.model.num_deaths += 1

    def gain_virus_immnunity(self):
        if self.random.random() < self.model.gain_immnunity_chance:
            self.state = HealthState.RESISTANT
                