import math
from enum import Enum

from mesa import Agent, Model
from mesa.time import SimultaneousActivation
from mesa.datacollection import DataCollector
from mesa.space import SingleGrid

from agent import Student
from state import ActivityState, HealthState

def number_state(model: Model, state: Enum):
    return sum([1 for a in model.get_agents() if a.state is state])

def number_infected(model: Model):
    return number_state(model, HealthState.INFECTED)

def number_exposed(model: Model):
    return  number_state(model, HealthState.EXPOSED)

def number_susceptible(model: Model):
    return number_state(model, HealthState.SUSCEPTIBLE)

def number_resistant(model: Model):
    return number_state(model, HealthState.RESISTANT)

def get_position_by_class(model: Model, i: int):
    width = model.WIDTH
    height = model.HEIGHT
    num_classes = model.num_classes
    class_size = int(model.population_size/model.num_classes)

    
    pos_in_class = int(i/num_classes)
    class_ = i%num_classes

    pos = class_ * class_size + pos_in_class
   
    x = int(pos/height)
    y = pos%height

    return x, y

class CovidTable(Model):
    '''
    This model simulates the expansion of the virus in an university.
    '''

    def __init__(self,
        population_size: int = 133,
        num_classes: int = 10,
        virus_spread_chance: float = 0.7,
        gain_immnunity_chance: float = 0.8,
        infection_recovery_chance: float = 0.03,
        activity_infection_chance: float = 0.01,
        lethal_infection_chance: float = 0.005,
        mean_incubation_time: int = 96, # In hours
        mask_probability: float = 0.9, # Probability that an individual is using a mask
        cafeteria_table_size: int = 8, # Number of individuals on each cafeteria table
    ):  
        #assert population_size%num_classes == 0
        self.WIDTH = 50
        self.HEIGHT = math.ceil(population_size/self.WIDTH)
        self.population_size = population_size
        self.num_classes=num_classes
        self.current_cycle_step = 0
        self.num_deaths = 0
        self.grid = SingleGrid(width=self.WIDTH, height=self.HEIGHT, torus=False)

        self.scheduler = SimultaneousActivation(self)
        self.datacollector = DataCollector(
            {
                'Infected': number_infected,
                'Exposed': number_exposed,
                'Susceptible': number_susceptible,
                'Resistant': number_resistant,
            }
        )
        self.prev_number_infected = 0
        self.prev_number_exposed = 0
        self.prev_number_susceptible = 0
        self.prev_number_resistant = 0

        self.virus_spread_chance = virus_spread_chance
        self.gain_immnunity_chance = gain_immnunity_chance
        self.infection_recovery_chance = infection_recovery_chance
        self.activity_infection_chance = activity_infection_chance
        self.lethal_infection_chance = lethal_infection_chance
        self.mean_incubation_time = mean_incubation_time
        self.cafeteria_table_size = cafeteria_table_size

        # Agent creation
        for i in range(population_size):
            pos = get_position_by_class(self, i)
            agent = Student(
                unique_id=i,
                model=self,
                x = pos[0],
                y = pos[1],
                initial_state=HealthState.SUSCEPTIBLE,
                initial_activity_state=ActivityState.AT_HOME,
                class_number=i%num_classes,
                use_mask=self.random.random() < mask_probability,
            )
            self.scheduler.add(agent)
            
            self.grid.position_agent(agent, x=pos[0], y=pos[1])

        self.running = True
        self.datacollector.collect(self)

    def get_agents(self):
        return self.scheduler.agents
    
    def infection_rate(self):
        ''' 
        The product of the mean number of contacts per individual
        per unit of time and the chance of infection in the contact
        between a Susceptible and a Infected individual.
        ''' 
        contacts = [a.contacts for a in self.get_agents() if a.state is HealthState.SUSCEPTIBLE]
        mean_contacts = sum(contacts) / len(contacts)
        return mean_contacts * self.virus_spread_chance
    
    def recovery_rate(self):
        '''
        The ratio between the number of resistant individuals and 
        the number of infected individuals per unit of time.
        '''
        try:
            return (number_resistant(self) - self.prev_number_resistant)/(number_infected(self)-self.prev_number_infected)
        except ZeroDivisionError:
            return math.inf

    def reproduction_rate(self):
        '''
        The ratio between the infection rate and the recovery rate
        '''
        try:
            return self.infection_rate()/self.recovery_rate()
        except ZeroDivisionError:
            return math.inf
        
    def step(self):
        self.current_cycle_step += 1
        self.prev_number_infected = number_infected(self)
        self.prev_number_exposed = number_exposed(self)
        self.prev_number_susceptible = number_susceptible(self)
        self.prev_number_resistant = number_resistant(self)

        self.scheduler.step()
        # collect data
        self.datacollector.collect(self)

    def run_model(self, n):
        for i in range(n):
            self.step()
