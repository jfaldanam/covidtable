import math

from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.UserParam import UserSettableParameter
from mesa.visualization.modules import CanvasGrid, ChartModule, TextElement


from model import CovidTable, number_exposed, number_infected, number_resistant, number_susceptible
from state import ActivityState, HealthState

class ShowCurrentDay(TextElement):
    def render(self, model):
        print(model.current_cycle_step)
        return f"Day: {math.floor(model.current_cycle_step/24)} Hour: {model.current_cycle_step%24}"

class HealthStateCounter(TextElement):
    def render(self, model):
        return f"<br> Susceptible: {number_susceptible(model)} Exposed: {number_exposed(model)} Infected: {number_infected(model)} Resistant alive: {number_resistant(model)-model.num_deaths} Deaths: {model.num_deaths}"

class ExplainVisualization(TextElement):
    def render(self, model):
        return f"<br><h3>Health state and class number</h3>"

class ExplainVisualization2(TextElement):
    def render(self, model):
        return "<br><h3>Activity state</h3>"

chart = ChartModule(
    [
        {"Label": "Susceptible", "Color": "#008000"},
        {"Label": "Exposed", "Color": "#00FFFF"},
        {"Label": "Infected", "Color": "#FF0000"},
        {"Label": "Resistant", "Color": "#808080"},
    ]
)

model_params = {
    "population_size": UserSettableParameter(
        "slider",
        "Population size",
        1000,
        50,
        10000,
        50,
        description="Number of individuals that are part of the simulation"
    ),
    "num_classes": UserSettableParameter(
        "slider",
        "Number of university classes",
        4,
        1,
        10,
        1,
        description="Number of classes to which individuals in the model can belong"
    ),
    "cafeteria_table_size": UserSettableParameter(
        "slider",
        "Individuals per table at the cafeteria",
        8,
        0,
        16,
        1,
        description="Number of individuals per table at the cafeteria"
    ),
    "virus_spread_chance": UserSettableParameter(
        "slider",
        "Virus spread %",
        0.7,
        0.0,
        1.0,
        0.01,
        description="Chance to spread the virus to contact individual",
    ),
    "gain_immnunity_chance": UserSettableParameter(
        "slider",
        "Gain immunity %",
        0.8,
        0.0,
        1.0,
        0.01,
        description="Chance to gain immunity to the virus after being infected",
    ),
    "infection_recovery_chance": UserSettableParameter(
        "slider",
        "Recover from infection %",
        0.03,
        0.0,
        1.0,
        0.01,
        description="Chance to recover from the virus while being infected",
    ),
    "activity_infection_chance": UserSettableParameter(
        "slider",
        "Activity infection %",
        0.01,
        0.0,
        1.0,
        0.01,
        description="Chance to being infected while doing an outside of class activity",
    ),
    "lethal_infection_chance": UserSettableParameter(
        "slider",
        "Lethal infection %",
        0.005,
        0.0,
        1.0,
        0.01,
        description="Chance to die from the virus while being infected",
    ),
    "mean_incubation_time": UserSettableParameter(
        "slider",
        "Mean incubation time (in hours)",
        96,
        1,
        100,
        1,
        description="Mean number of hours that pass between a individual being exposed to the virus and getting symptomps",
    ),
    "mask_probability": UserSettableParameter(
        "slider",
        "% of population using masks",
        0.9,
        0.0,
        1.0,
        0.01,
        description="Percetage of the population using a mask",
    ),
}

def agent_portrayal(agent):
    if agent.state == HealthState.INFECTED:
        color = "#FF0000"
    elif agent.state == HealthState.EXPOSED:
        color = "#00FFFF"
    elif agent.state == HealthState.SUSCEPTIBLE:
        color = "#008000"
    elif agent.state == HealthState.RESISTANT:
        color = "#808080"

    portrayal = {"Shape": "rect",
                "Filled": "true",
                "Layer": 0,
                "Color": color,
                "w": 1,
                "h":1,
                "text": agent.class_number,
                "text_color":"black"}

    return portrayal

def activity_portrayal(agent):
    if agent.activity_state == ActivityState.AT_HOME:
        shape = "icon/home.png"
    elif agent.activity_state == ActivityState.AT_CLASS:
        shape = "icon/class.png"
    elif agent.activity_state == ActivityState.AT_CAFETERIA:
        shape = "icon/cafeteria.png"
    elif agent.activity_state == ActivityState.AT_ACTIVITY:
        shape = "icon/activity.png"

    portrayal = {
        "Shape": shape,
        "Layer": 0,
    }

    return portrayal

class RateTextElement(TextElement):
    def render(self, model):
        infection_ratio = model.infection_rate()
        infection_ratio_text = "&infin;" if infection_ratio is math.inf else "{0:.2f}".format(infection_ratio)
        recovery_ratio = model.recovery_rate()
        recovery_ratio_text = "&infin;" if recovery_ratio is math.inf else "{0:.2f}".format(recovery_ratio)
        reproduction_ratio = model.reproduction_rate()
        reproduction_ratio_text = "&infin;" if reproduction_ratio is math.inf else "{0:.2f}".format(reproduction_ratio)
        return f"Infection Rate: {infection_ratio_text} Recovery Rate: {recovery_ratio_text} Reproduction Rate: {reproduction_ratio_text}"


health_grid = CanvasGrid(agent_portrayal, 50, 20, 800, 300)
activity_grid = CanvasGrid(activity_portrayal, 50, 20, 800, 300)
server = ModularServer(
    CovidTable, [ShowCurrentDay(), RateTextElement(), chart, HealthStateCounter(), ExplainVisualization(), health_grid, ExplainVisualization2(), activity_grid], "Covid table", model_params
)
server.port = 8521