from enum import Enum

class HealthState(Enum):
    '''
    This enum defines the posibles state of the SEIR model.
    '''
    # Not-infected people that can be infected
    SUSCEPTIBLE = 0
    # Infected individuals that don't have symptoms and can't infect others
    EXPOSED = 1
    # Individuals porting the disease
    INFECTED = 2
    # Individuals that have passed (or fallen) to the disease
    RESISTANT = 3


class ActivityState(Enum):
    '''
    This enum defines the posible activity an agent
    can be doing during a day.
    '''
    AT_HOME = 0

    AT_CLASS = 1

    AT_CAFETERIA = 2

    AT_ACTIVITY = 3
